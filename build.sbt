import com.typesafe.sbt.SbtScalariform
import scalariform.formatter.preferences.{PlaceScaladocAsterisksBeneathSecondAsterisk, DoubleIndentClassDeclaration, AlignSingleLineCaseStatements}


lazy val flinkSbtSeed = (project in file("."))
  .settings(

    organization := "org.example",
    name         := "flink-sbt-seed",
    version      := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.7",
    licenses     += ("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0")),

    resolvers += "apache-snapshots" at "https://repository.apache.org/content/repositories/snapshots/",

    libraryDependencies ++= Vector(
      "org.apache.flink"    % "flink-scala_2.11"    % "0.10-SNAPSHOT",
      "org.apache.flink"    % "flink-clients_2.11"  % "0.10-SNAPSHOT",
      "org.scalatest"      %% "scalatest"           % "2.2.6"           % Test
    ),

    scalacOptions ++= Vector(
      "-unchecked",
      "-deprecation",
      "-language:_",
      "-target:jvm-1.8",
      "-encoding", "UTF-8"
    ),

    // application should be run in a separate jvm
    // see https://ci.apache.org/projects/flink/flink-docs-release-0.10/quickstart/scala_api_quickstart.html
    fork in run := true,

    // plugin settings

    // scalariform
    SbtScalariform.scalariformSettings ++ Vector(
      ScalariformKeys.preferences := ScalariformKeys.preferences.value
        .setPreference(AlignSingleLineCaseStatements, true)
        .setPreference(AlignSingleLineCaseStatements.MaxArrowIndent, 100)
        .setPreference(DoubleIndentClassDeclaration, true)
        .setPreference(PlaceScaladocAsterisksBeneathSecondAsterisk, true)
    )

  )
