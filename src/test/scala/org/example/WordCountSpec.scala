package org.example

import org.apache.flink.api.scala.ExecutionEnvironment
import org.scalatest.{ Matchers, WordSpec }

class WordCountSpec extends WordSpec with Matchers {

  implicit val env = ExecutionEnvironment.getExecutionEnvironment

  val wordCount = new WordCount()

  "WordCount" should {

    "return the correct number of occurrences" in {

      val text = "apple, apple, banana, strawberry, apple, banana"

      val expectedResult = Vector(
        ("apple", 3),
        ("banana", 2),
        ("strawberry", 1))

      val wordCountResult = wordCount.countOccurences(text)

      wordCountResult should contain theSameElementsAs expectedResult

    }

  }

}
