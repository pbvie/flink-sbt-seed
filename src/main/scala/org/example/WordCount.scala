package org.example

import org.apache.flink.api.scala._

class WordCount {

  def countOccurences(text: String)(implicit env: ExecutionEnvironment): Seq[(String, Int)] = {
    val ds = env.fromElements(text)

    ds.flatMap(_.toLowerCase.split("\\W+").filter(_.nonEmpty))
      .map((_, 1))
      .groupBy(0)
      .sum(1)
      .collect()
  }

}