package org.example

import org.apache.flink.api.scala._

object WordCountApp {

  def main(args: Array[String]): Unit = {

    implicit val env = ExecutionEnvironment.getExecutionEnvironment

    val text =
      """
        |Apache Flink is an open source platform for distributed stream and batch data processing.
        |
        |Flink’s core is a streaming dataflow engine that provides data distribution, communication, and fault tolerance for distributed computations over data streams.
        |
        |Flink includes several APIs for creating applications that use the Flink engine:
        |
        |DataSet API for static data embedded in Java, Scala, and Python,
        |DataStream API for unbounded streams embedded in Java and Scala, and
        |Table API with a SQL-like expression language embedded in Java and Scala.
        |
        |Flink also bundles libraries for domain-specific use cases:
        |
        |Machine Learning library, and
        |Gelly, a graph processing API and library.
        |
      """.stripMargin

    val wordCount = new WordCount()
    val result = wordCount.countOccurences(text)

    result.foreach(println)
  }

}
