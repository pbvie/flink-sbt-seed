# flink-sbt-seed #

A minimal seed template for an sbt project with Apache Flink.

## Versions

* Scala 2.11.7
* Apache Flink 0.10-SNAPSHOT
* sbt 0.13.9

The 0.10.1 release of Apache Flink contains the following bug when using Scala 2.11 and Java 8: https://issues.apache.org/jira/browse/FLINK-3143

For this reason the snapshot version was used.

## Contribution policy ##

Contributions via GitHub pull requests are gladly accepted from their original author. Along with any pull requests, please state that the contribution is your original work and that you license the work to the project under the project's open source license. Whether or not you state this explicitly, by submitting any copyrighted material via pull request, email, or other means you agree to license the material under the project's open source license and warrant that you have the legal authority to do so.

## License ##

This code is open source software licensed under the [Apache 2.0 License]("http://www.apache.org/licenses/LICENSE-2.0.html").